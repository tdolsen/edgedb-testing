import { Controller, Get } from "@nestjs/common";

import { EdgeDBService } from "~/edgedb";
import e from "~/generated/edgeql";

@Controller()
export class AppController {
	constructor(private readonly $edgedb: EdgeDBService) {}

	@Get()
	getHello() {
		return this.$edgedb.query(
			e.select(e.Product, () => ({
				id: true,
				name: true,
			})),
		);
	}
}
