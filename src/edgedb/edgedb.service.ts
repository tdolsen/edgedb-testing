import { createClient } from "edgedb";
import { Expression } from "edgedb/dist/reflection";

export class EdgeDBService {
	private client = createClient();

	public async query(ee: Expression) {
		return ee.run(this.client);
	}
}
