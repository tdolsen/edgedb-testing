import { Module } from "@nestjs/common";

import { EdgeDBModule } from "~/edgedb";

import { AppController } from "./app.controller";
import { AppService } from "./app.service";

@Module({
	imports: [EdgeDBModule],
	controllers: [AppController],
	providers: [AppService],
})
export class AppModule {}
