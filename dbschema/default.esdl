module default {
	type User {
		required property email -> str;
		multi link purchases -> Purchase;
	}

	type Product {
		required property productId -> str;
		required property name -> str;
	}

	scalar type PurchaseNumber extending sequence;
	type Purchase {
		required property number -> PurchaseNumber;
		required property time -> datetime;
		required link user -> User;
		required link product -> Product;
	}
}
