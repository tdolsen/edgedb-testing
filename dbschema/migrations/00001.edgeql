CREATE MIGRATION m16u26fohqa3nzfwtmw4dzhyhzqqxrkvsezfifguv63jqgeg3syn2q
    ONTO initial
{
  CREATE TYPE default::Product {
      CREATE REQUIRED PROPERTY name -> std::str;
      CREATE REQUIRED PROPERTY productId -> std::str;
  };
  CREATE SCALAR TYPE default::PurchaseNumber EXTENDING std::sequence;
  CREATE TYPE default::Purchase {
      CREATE REQUIRED LINK product -> default::Product;
      CREATE REQUIRED PROPERTY number -> default::PurchaseNumber;
      CREATE REQUIRED PROPERTY time -> std::datetime;
  };
  CREATE TYPE default::User {
      CREATE MULTI LINK purchases -> default::Purchase;
      CREATE REQUIRED PROPERTY email -> std::str;
  };
  ALTER TYPE default::Purchase {
      CREATE REQUIRED LINK user -> default::User;
  };
};
